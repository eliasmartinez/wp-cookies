const cookieMessageI18n = {
	es: 'Utilizamos cookies propias y de terceros para fines analíticos y para mostrarte publicidad personalizada en base a un perfil elaborado a partir de tus hábitos de navegación (por ejemplo, páginas visitadas). Clica aquí para más información. Puedes aceptar todas las cookies pulsando el botón "Aceptar" o configurarlas o rechazar su uso pulsando el botón "Configurar".',
	en: 'We use our own and third-party cookies for analytical purposes and to show you personalized advertising based on a profile made from your browsing habits (for example, pages visited). Click here for more information. You can accept all cookies by pressing the "Accept" button or configure or reject their use by pressing the "Configure" button.',
	fr: 'Nous utilisons nos propres cookies et ceux de tiers à des fins d\'analyse et pour vous montrer des publicités personnalisées en fonction d\'un profil établi à partir de vos habitudes de navigation (par exemple, les pages visitées). Cliquez ici pour plus d\'informations. Vous pouvez accepter tous les cookies en appuyant sur le bouton "Accepter" ou configurer ou refuser leur utilisation en appuyant sur le bouton "Configurer".',
	ru: 'Мы используем наши собственные и сторонние файлы cookie в аналитических целях и для показа вам персонализированной рекламы на основе профиля, составленного на основе ваших привычек просмотра (например, посещенных страниц). Для получения дополнительной информации нажмите здесь. Вы можете принять все файлы cookie, нажав кнопку «Принять», или настроить или отклонить их использование, нажав кнопку «Настроить».',
};

const configurationBtnI18n = {
	es: 'Configurar cookies',
	en: 'Set cookies',
	fr: 'Définir les cookies',
	ru: 'Установить куки'
};

const acceptBtnI18n = {
	es: 'Aceptar todas las cookies',
	en: 'Accept all cookies',
	fr: 'Acceptez tous les cookies',
	ru: 'Принимать все куки'
};

const cookiesConfigMessageI18n = {
	es: 'Aquarama le informa que este sitio web utiliza cookies. A continuación, encontrará información detallada sobre qué son las Cookies, qué tipología de cookies trata esta web y cómo configurarlas. Se considera cookies a cualquier tipo de archivo o dispositivo que se descarga en el equipo terminal de un usuario con la finalidad de almacenar datos que podrán ser actualizados y recuperados por la entidad responsable de su instalación. Se ubican en el disco duro del ordenador o terminal del Usuario, que archivan información sobre el uso que se realiza de un determinado Sitio Web. Las cookies solamente pueden ser leídas por el servidor que las colocó y no pueden ejecutar ningún programa o virus.',
	en: 'Aquarama informs you that this website uses cookies. Below, you will find detailed information about what Cookies are, what type of cookies this website deals with and how to configure them. Cookies are any type of file or device that is downloaded to a user\'s terminal equipment in order to store data that can be updated and retrieved by the entity responsible for its installation. They are located on the hard drive of the User\'s computer or terminal, which archive information about the use made of a certain Website. Cookies can only be read by the server that placed them and cannot run any program or virus.',
	fr: 'Aquarama vous informe que ce site utilise des cookies. Vous trouverez ci-dessous des informations détaillées sur ce que sont les cookies, sur le type de cookies que traite ce site Web et sur la manière de les configurer. Les cookies sont considérés comme tout type de fichier ou d\'appareil téléchargé sur l\'équipement terminal d\'un utilisateur afin de stocker des données pouvant être mises à jour et récupérées par l\'entité responsable de son installation. Ils sont situés sur le disque dur de l\'ordinateur ou du terminal de l\'Utilisateur, qui archivent des informations sur l\'utilisation faite d\'un certain site Web. Les cookies ne peuvent être lus que par le serveur qui les a placés et ne peuvent exécuter aucun programme ou virus.',
	ru: 'Aquarama сообщает вам, что этот веб-сайт использует файлы cookie. Ниже вы найдете подробную информацию о том, что такое файлы cookie, с какими типами файлов cookie работает этот веб-сайт и как их настроить. Куки-файлы считаются любым типом файла или устройства, которые загружаются на оконечное оборудование пользователя для хранения данных, которые могут обновляться и извлекаться лицом, ответственным за их установку. Они расположены на жестком диске компьютера или терминала Пользователя, в которых хранится информация об использовании определенного Веб-сайта. Файлы cookie могут быть прочитаны только сервером, который их разместил, и не может запускать какие-либо программы или вирусы.'
};

const cookiesConfigRequiredInfoI18n = {
	es: 'Cookies estrictamente necesarias',
	en: 'Strictly necessary cookies',
	fr: 'Cookies strictement nécessaires',
	ru: 'Строго необходимые файлы cookie'
};

const cookiesAlwaysActive = {
	es: 'Siempre activo',
	en: 'Always active',
	fr: 'Toujours actif',
	ru: 'Всегда активный'
};

const cookiesConfigRequiredMessageI18n = {
	es: 'Estas cookies son necesarias para que el sitio web funcione y no se pueden desactivar en nuestros sistemas. Por lo general, solo se configuran en respuesta a sus acciones realizadas al solicitar servicios, como establecer sus preferencias de privacidad, iniciar sesión o completar formularios. Puede configurar su navegador para bloquear o alertar sobre estas cookies, pero algunas áreas del sitio no funcionarán. Estas cookies no almacenan ninguna información de identificación personal.',
	en: 'These cookies are necessary for the operation of the website and for the purpose of deactivating our systems. It is generally defined that responds to your actions between the request for services, that the definition of your confidentiality, the connection or the filling of forms. You can configure your browser to block or alert about cookies, more certain zones of the site and source. Ces cookies ne stockent aucune information personnellement identifiable.',
	fr: 'Ces cookies sont nécessaires au fonctionnement du site Web et ne peuvent pas être désactivés dans nos systèmes. Ils ne sont généralement définis qu\'en réponse à vos actions entreprises lors de la demande de services, telles que la définition de vos préférences de confidentialité, la connexion ou le remplissage de formulaires. Vous pouvez configurer votre navigateur pour bloquer ou alerter sur ces cookies, mais certaines zones du site ne fonctionneront pas. Ces cookies ne stockent aucune information personnellement identifiable.',
	ru: 'Эти файлы cookie необходимы для работы веб-сайта и не могут быть отключены в наших системах. Обычно они устанавливаются только в ответ на ваши действия, предпринятые при запросе услуг, такие как настройка параметров конфиденциальности, вход в систему или заполнение форм. Вы можете настроить свой браузер так, чтобы он блокировал эти файлы cookie или предупреждал о них, но некоторые области сайта не будут работать. Эти файлы cookie не хранят никакой личной информации.'
};

const cookiesConfigPerformanceInfoI18n = {
	es: 'Cookies de rendimiento, análisis, personalización y publicidad',
	en: 'Performance, analysis, personalization and advertising cookies',
	fr: 'Cookies de performance, d\'analyse, de personnalisation et de publicité',
	ru: 'Производительность, анализ, персонализация и рекламные файлы cookie'
};

const cookiesActivateI18n = {
	es: 'Activar',
	en: 'Activate',
	fr: 'Activer',
	ru: 'Активировать'
};

const cookiesPerformanceMessageI18n = {
	es: 'Estas cookies nos permiten contar las visitas y fuentes de tráfico para poder evaluar el rendimiento de nuestro sitio y mejorarlo. Nos ayudan a saber qué páginas son las más o las menos visitadas, y cómo los visitantes navegan por el sitio. Toda la información que recogen estas cookies es agregada y, por lo tanto, es anónima. Si no permite utilizar estas cookies, no sabremos cuándo visitó nuestro sitio y no podremos evaluar si funcionó correctamente.',
	en: 'These cookies allow us to count visits and traffic sources in order to evaluate the performance of our site and improve it. They help us know which pages are the most or least visited, and how visitors navigate the site. All the information that these cookies collect is aggregated and, therefore, is anonymous. If you do not allow these cookies, we will not know when you visited our site and we will not be able to evaluate whether it worked correctly.',
	fr: 'Ces cookies nous permettent de compter les visites et les sources de trafic afin d\'évaluer les performances de notre site et de l\'améliorer. Ils nous aident à savoir quelles pages sont les plus ou les moins visitées et comment les visiteurs naviguent sur le site. Toutes les informations que ces cookies collectent sont agrégées et, par conséquent, sont anonymes. Si vous n\'autorisez pas ces cookies, nous ne saurons pas quand vous avez visité notre site et nous ne serons pas en mesure d\'évaluer si cela a fonctionné correctement.',
	ru: 'Эти файлы cookie позволяют нам подсчитывать посещения и источники трафика, чтобы оценивать производительность нашего сайта и улучшать ее. Они помогают нам узнать, какие страницы наиболее или наименее посещаемые и как посетители перемещаются по сайту. Вся информация, которую собирают эти файлы cookie, является агрегированной и, следовательно, анонимной. Если вы не разрешите использование этих файлов cookie, мы не узнаем, когда вы посетили наш сайт, и не сможем оценить, правильно ли он работал ».'
};

const cookiesAcceptConfiguration = {
	es: 'Aceptar configuración',
	en: 'Accept configuration',
	fr: 'Accepter la configuration',
	ru: 'Принять конфигурацию'
}

if (window.addEventListener) {
	window.addEventListener('load', init_web_cookie_checker);
} else {
	window.attachEvent('onload', init_web_cookie_checker);
}

function get_current_lang() {
	const htmlDocument = document.getElementsByTagName('html')[0];
	const currentLang = htmlDocument.getAttribute('lang');
	return currentLang.substring(0, 2);
}

function set_cookies(enablePerformanceCookies = true) {
	const c_name = "web_cookie_checker";
	const value = "1";
	const exdays = 20;

	const exdate = new Date();
	exdate.setDate ( exdate.getDate() + exdays );
	const c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;

	if ( localStorage ) {
		localStorage.web_cookie_checker = 1;

		if ( enablePerformanceCookies )
			localStorage.web_cookie_performance = 1;
	}
}

function is_cookie_saved() {
	return localStorage.web_cookie_checker;
}

function init_web_cookie_checker() {

	create_configuration_cookies_modal();

	if (!is_cookie_saved()) {
		const lang = get_current_lang();
		const message = cookieMessageI18n[lang];

		const configurationBtn = (`
			<button 
				class="config-cookies"
				data-toggle="modal"
				data-target="#cookies-modal-configuration"
				onClick="on_config_cookies()"
			>
				${configurationBtnI18n[lang]}
			</button>`
		);

		const acceptBtn = (
			`<button 
				class="accept-cookies"
				onClick="on_accept_cookies(true); hide_cookies_modal();"
			>
				${acceptBtnI18n[lang]}
			</button>`
		);

		const content = (
			`<div id="cookies-modal-container">
					<div class="message">
                        ${message}
                    </div>
                    <div class="actions">
                        ${configurationBtn}
                        ${acceptBtn}
                    </div>
            </div>`
		);

		const div = document.createElement ( 'div' );
		div.innerHTML = content;
		document.body.appendChild ( div );
	}
}

function on_config_cookies() {
	jQuery('#cookies-modal-container').hide();
}

function hide_cookies_modal() {
	jQuery('#cookies-modal-container').hide();
}

function on_accept_cookies(performanceCookies) {
	set_cookies(performanceCookies);
}

function create_configuration_cookies_modal () {
	const lang = get_current_lang();

	const cookiesModalConfiguration = document.createElement('div');
	cookiesModalConfiguration.innerHTML = (
		`
		<div class="modal fade" id="cookies-modal-configuration" data-backdrop="static" tabindex="-1" role="dialog">
        	<div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                	<div class="modal-header">
                 		<h4 class="modal-title">${configurationBtnI18n[lang]}</h4>
                    </div>

                    <div class="modal-body">
                        <p>${cookiesConfigMessageI18n[lang]} </p>
                        <h5 class="cookie-header">
                            ${cookiesConfigRequiredInfoI18n[lang]}: <span class="badge badge-secondary">${cookiesAlwaysActive[lang]}</span>
                        </h5>
                        <p>${cookiesConfigRequiredMessageI18n[lang]}</p>
                        <h5 class="cookie-header">
                            ${cookiesConfigPerformanceInfoI18n[lang]}:
                            <span class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="accept-cookies">
                            <label class="custom-control-label" for="accept-cookies"/>
                        </span>
                        <span class="switch-label">
                            ${cookiesActivateI18n[lang]}
                        </span>
                        </h5>
                        <p>${cookiesPerformanceMessageI18n[lang]}</p>
                    </div>

                    <div class="modal-footer">
                    	<button 
                    		class="btn btn-secondary" 
                    		onClick="on_accept_cookies(jQuery('#accept-cookies').prop('checked'))"
                    		data-dismiss="modal"
                    	>
							${cookiesAcceptConfiguration[lang]}
						</button>
                    </div>
                </div>
            </div>
        </div>
		`
	);

	document.body.appendChild(cookiesModalConfiguration);
}
